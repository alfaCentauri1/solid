# SOLID
Ejemplo del principio Solid aplicado en Java.

## Requisitos
* JDK 1.8.
* Maven.

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ingrese por consola a la raíz del proyecto.
* Ejecute en la consola **java -jar target/ejemplocamel5.jar**

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

*** 

## Test
Existen pruebas unitarias. Ver en la carpeta de test.

## Project status
* Developer