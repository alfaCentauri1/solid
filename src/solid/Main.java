package solid;

public class Main {
    public static void main(){
        ITeclado teclado = new Teclado();
        IMouse mouse = new Mouse();
        Computadora computadora = new Computadora(teclado, mouse);
        computadora.encender();
    }
}
