package solid;

public class Computadora{
    private ITeclado teclado;
    private IMouse mouse;

    public Computadora(ITeclado teclado, IMouse mouse) {
        this.teclado = teclado;
        this.mouse = mouse;
    }

    public ITeclado getTeclado() {
        return teclado;
    }

    public void setTeclado(ITeclado teclado) {
        this.teclado = teclado;
    }

    public IMouse getMouse() {
        return mouse;
    }

    public void setMouse(IMouse mouse) {
        this.mouse = mouse;
    }

    public void encender(){
        this.teclado.conectar();
        this.mouse.conectar();
    }
}
