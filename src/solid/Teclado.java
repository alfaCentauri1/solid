package solid;

public class Teclado implements ITeclado {
    @Override
    public void conectar() {
        System.out.println("Se conectó el teclado.");
    }
}
