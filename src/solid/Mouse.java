package solid;

public class Mouse implements IMouse {
    @Override
    public void conectar() {
        System.out.println("Se conectó el mouse.");
    }
}
